import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path';
import path from 'path'

const pathResolve = (dir: string): any => {
  return resolve(__dirname, '.', dir);
};
const alias: Record<string, string> = {
  '/@': pathResolve('./src/'),
  'vue-i18n': 'vue-i18n/dist/vue-i18n.cjs.js',
};
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue()
  ],
  // Vite启动后提示“Network: use `--host` to expose“ 通过ip访问
  server: {
    host: '0.0.0.0',
    proxy: {
      '/api': {
        target: 'http://192.168.128.66:8081',
        ws: false,
        changeOrigin: true,
        rewrite: path => path.replace(/^\/api/, '')
      }
    }
  },
  resolve: {
    // 配置别名 vue cli一样 以@引入文件
    alias: {
      '/@': pathResolve('./src/'),
      // '@': path.join(__dirname, './src'),
      // '@components': path.join(__dirname, './src/components'),
      // '@utils': path.join(__dirname, './src/utils'),
      // '@router': path.join(__dirname, './src/router'),
      // '@request': path.join(__dirname, './src/utils/request'),
      // '@store': path.join(__dirname, './src/store'),
      // '@storage': path.join(__dirname, './src/utils/storage'),
      // '@model': path.join(__dirname, './src/utils/model'),
      // '@service': path.join(__dirname, './src/api/service'),
      // '@handler': path.join(__dirname, './src/api/handler'),
    }
  }
})
