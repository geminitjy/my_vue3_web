import request from '../../utils/request';

/**
 * 用户登录
 * params 需要传的参数
 */
export function LoginIn(params: object) {
    return request({
        url: '/HgLogin/api/program/login',
        method: 'post',
        data: params,
    });
}
