import { Module } from 'vuex';
import { Session } from '/@/utils/storage';
import { UserInfosState, RootStateTypes } from '/@/store/interface/index';

const userInfosModule: Module<UserInfosState, RootStateTypes> = {
    namespaced: true,
    state: {
        userInfos: {},  /** 用戶信息  */
        // 路由菜单
        menus: [],
        // token
        token: "",
        // 用户信息
        account: {}
    },
    mutations: {
        // 设置用户信息
        getUserInfos(state: any, data: object) {
            state.userInfos = data;
        },
    //    获取路由信息
        getMenu(state:any, data:any ){
            state.menus = data
        }
    },
    actions: {
        // 设置用户信息
        async setUserInfos({ commit }, data: object) {
            if (data) {
                commit('getUserInfos', data);
            } else {
                if (Session.get('userInfos')) commit('getUserInfos', Session.get('userInfos'));
            }
        },
         setMlistinfos({ commit } ,data:any){
            if(data){
                commit('getMenu',data)
            } else {
                if(Session.get('menuList')) commit('getMenu',Session.get('menuList'))
            }
        }
    },
};

export default userInfosModule;
