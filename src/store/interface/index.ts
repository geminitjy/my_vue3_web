// 用户信息
export interface UserInfosState {
    userInfos: object;
}
// 主接口(顶级类型声明)
export interface RootStateTypes {
    // themeConfig: ThemeConfigState;
    // routesList: RoutesListState;
    // keepAliveNames: KeepAliveNamesState;
    // tagsViewRoutes: TagsViewRoutesState;
    userInfos: UserInfosState;
    // requestOldRoutes: RequestOldRoutesState;
}
