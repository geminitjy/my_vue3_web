import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import { store, key } from './store';

import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import '/@/theme/index.scss';
import mitt from 'mitt';
import * as ElIcon from '@element-plus/icons-vue'
const app = createApp(App);
for (let iconName in ElIcon) {
    app.component(iconName, ElIcon[iconName])
}



app
    .use(router)
    .use(store, key)
    .use(ElementPlus,)
    .mount('#app');

app.config.globalProperties.mittBus = mitt();
