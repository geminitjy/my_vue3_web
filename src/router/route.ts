import {RouteRecordRaw} from 'vue-router';


export const dynamicRoutes: { redirect: string; path: string; component: () => any; meta: { isKeepAlive: boolean }; name: string }[] = [

    {
        path: '/',
        name: '/',
        component: () => import('/@/layout/index.vue'),
        redirect: '/personal',
        meta: {
            isKeepAlive: true,
        },
    },

];

export const staticRoutes: { path: string; component: any; meta: { title: string } }[] = [
    {
        path: "/login",
        component: () => import('/@/page/login/index.vue'),
        meta: {
            title: "登录页",
        },
    },
];
